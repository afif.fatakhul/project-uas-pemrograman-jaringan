from django.shortcuts import render, redirect, HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout

# Create your views here.

var = {
    'judul' : 'Halaman Home | Penugasan Kemenag Kab Jombang',
}

@login_required(login_url='login')
def index(self):
    return render(self, 'dashboard/index.html', context=var)

def loginPage(request):
	if request.user.is_authenticated:
		return redirect('/')
	else:
		if request.method == 'POST':
			username = request.POST.get('username')
			password =request.POST.get('password')

			user = authenticate(request, username=username, password=password)

			if user is not None:
				login(request, user)
				return redirect('/')
			else:
				messages.error(request, 'Username atau password salah')

		context = {}
		return render(request, 'dashboard/login.html', context)

@login_required(login_url='login')
def logoutUser(request):
	logout(request)
	return redirect('/login')