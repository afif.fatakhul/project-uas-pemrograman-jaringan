from django.urls import path
from .views import *
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('penugasan',index,name='penugasan'),
    path('penugasan/tambah',tambah_penugasan,name='tambah_tugas'),
    path('penugasan/edit/<int:id_tugas>', ubah_penugasan, name='ubah_tugas'),
    path('penugasan/hapus/<int:id_tugas>',hapus_tugas, name='hapus_tugas'),
    path('penugasan/print_pdf', TugasToPdf.as_view(), name='tugas_to_pdf')
]


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)