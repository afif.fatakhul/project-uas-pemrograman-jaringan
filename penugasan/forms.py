from django import forms
from django.forms import ModelForm
from .models import Penugasan

class FormPenugasan(ModelForm):
	class Meta:
		model = Penugasan
		fields = '__all__'

		widgets = {
            'tgl_tugas' : forms.DateTimeInput({'class':'form-control'}),
			'tgl_kembali' : forms.DateTimeInput({'class':'form-control'}),
            'no_surat' : forms.TextInput({'class':'form-control'}),
            'perihal' : forms.Textarea({'class':'form-control'}),
			'tempat_tugas' : forms.TextInput({'class':'form-control'}),
            'jenis_angkutan' : forms.Select({'class':'form-select'}),
            'jenis_tugas' : forms.Select({'class':'form-select'}),
            'pegawai_id' : forms.Select({'class':'form-select'}),
		}