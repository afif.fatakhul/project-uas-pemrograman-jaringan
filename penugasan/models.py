from django.db import models
from django.utils import timezone
from pegawai.models import Pegawai

class Penugasan(models.Model):
    JENIS_ANGKUTAN = (
        ('prbd','Pribadi'),
        ('umn','Umum'),
        ('dns','Dinas'),
    )

    JENIS_TUGAS = (
        ('lr','Luar'),
        ('dl','Dalam')
    )

    tgl_tugas = models.DateTimeField('Tanggal Tugas',null=False)
    tgl_kembali = models.DateTimeField('Tanggal Kembali',null=False)
    no_surat = models.CharField('No Surat',max_length=100, null=False)
    perihal = models.TextField(null=False)
    tempat_tugas = models.CharField('Tempat Tugas',max_length=255, null=False)
    jenis_angkutan = models.CharField(max_length=20, choices=JENIS_ANGKUTAN)
    jenis_tugas = models.CharField(max_length=20, choices=JENIS_TUGAS)
    file_tugas = models.FileField(upload_to='file/', null=False)
    pegawai_id = models.ForeignKey(Pegawai, related_name='ditugaskan',on_delete=models.DO_NOTHING)

class Meta:
    ordering = ['-tgl_tugas']

def __str__(self):
        return self.no_surat

