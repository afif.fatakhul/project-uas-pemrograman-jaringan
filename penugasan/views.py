from django.shortcuts import render, redirect, HttpResponse
from .models import Penugasan
from .forms import FormPenugasan
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.views.generic import DetailView

var = {
    'judul' : 'Penugasan Kemenag Kab Jombang',
}

@login_required(login_url='login')
def index(self):
    var['tugas'] = Penugasan.objects.values('id','tgl_tugas','tgl_kembali','no_surat','perihal','tempat_tugas','jenis_angkutan','jenis_tugas','file_tugas','pegawai_id_id')
    return render(self, 'penugasan/index.html', context=var)

@login_required(login_url='login')
def tambah_penugasan(request):
	form = FormPenugasan()
	if request.POST:
		form = FormPenugasan(request.POST, request.FILES)
		if form.is_valid():
			form.save()
			no_surat = form.cleaned_data.get('no_surat')
			messages.success(request, 'Berhasil menambah data penugasan : ' + no_surat)
			return redirect('/penugasan')

		context = {'form':form}
		return render(request, 'penugasan/add_tugas.html', context)

	else:
		form = FormPenugasan()
		judul = "Tambah Penugasan"

		context = {
			'title': judul,
			'form': form,
			'judul': 'Tambah Penugasan',
		}
	return render(request, 'penugasan/add_tugas.html', context)

@login_required(login_url='login')
def ubah_penugasan(request, id_tugas):
	tugas = Penugasan.objects.get(id=id_tugas)
	template = 'penugasan/edit_tugas.html'
	if request.POST:
		form = FormPenugasan(request.POST, instance=tugas)
		if form.is_valid():
			form.save()
			penugasan = form.cleaned_data.get('no_surat')
			messages.success(request, 'Berhasil mengubah data penugasan : ' + penugasan)
			return redirect('/penugasan')
	else:
		form = FormPenugasan(instance=tugas)
		judul = "Ubah Penugasan"
		context = {
			'form': form,
			'penugasan': tugas,
			'title': judul,
			'judul': 'Ubah Penugasan',
		}
	return render(request, template, context)

@login_required(login_url='login')
def hapus_tugas(request, id_tugas):
	penugasan = Penugasan.objects.filter(id=id_tugas)
	penugasan.delete()

	messages.success(request, 'Berhasil menghapus data penugasan')
	return redirect('/penugasan')


from .utils import Render
from django.views.generic import View
class TugasToPdf(View):
	def get(self, request):
		var = {
			'tugas' : Penugasan.objects.values('id','tgl_tugas','tgl_kembali','no_surat','perihal','tempat_tugas','jenis_angkutan','jenis_tugas','file_tugas','pegawai_id_id'),
			'request' : request
		} 
		return Render.to_pdf(self,'penugasan/print_pdf.html',var)