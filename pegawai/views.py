from django.shortcuts import render, redirect, HttpResponse
from .models import Pegawai
from .forms import FormPegawai
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.views.generic import CreateView, UpdateView, DeleteView

# Create your views here.
var = {
    'judul' : 'Penugasan Kemenag Kab Jombang',
}

@login_required(login_url='login')
def index(self):
    var['pegawai'] = Pegawai.objects.values('id','nip','nama','jabatanid','alamat','foto').\
        order_by('nama')
    return render(self, 'pegawai/index.html', context=var)

@login_required(login_url='login')
def tambah_pegawai(request):
	form = FormPegawai()
	if request.POST:
		form = FormPegawai(request.POST, request.FILES)
		if form.is_valid():
			form.save()
			pegawai = form.cleaned_data.get('nama')
			messages.success(request, 'Berhasil menambah data pegawai : ' + pegawai)
			return redirect('/pegawai')

		context = {'form':form}
		return render(request, 'pegawai/add_pegawai.html', context)

	else:
		form = FormPegawai()
		judul = "Tambah Pegawai"

		context = {
			'title': judul,
			'form': form,
		}
	return render(request, 'pegawai/add_pegawai.html', context)

@login_required(login_url='login')
def hapus_pegawai(request, id_pegawai):
	pegawai = Pegawai.objects.filter(id=id_pegawai)
	pegawai.delete()

	messages.success(request, 'Berhasil menghapus data pegawai')
	return redirect('/pegawai')

@login_required(login_url='login')
def ubah_pegawai(request, id_pegawai):
	pegawai = Pegawai.objects.get(id=id_pegawai)
	template = 'pegawai/edit_pegawai.html'
	if request.POST:
		form = FormPegawai(request.POST, instance=pegawai)
		if form.is_valid():
			form.save()
			pegawai = form.cleaned_data.get('nama')
			messages.success(request, 'Berhasil mengubah data pegawai : ' + pegawai)
			return redirect('/pegawai')
	else:
		form = FormPegawai(instance=pegawai)
		judul = "Ubah Pegawai"
		context = {
			'form': form,
			'pegawai': pegawai,
			'title': judul,
		}
	return render(request, template, context)