from django.db import models
from django.utils import timezone

class Pegawai(models.Model):
    JABATAN_CHOICES = (
        ('kpl','Kepala Dinas'),
        ('ks','Kasi'),
        ('kb','Kabib'),
    )

    nip = models.CharField('NIP Pegawai',max_length=50, null=False)
    nama = models.CharField('Nama Pegawai',max_length=100,null=False)
    jabatanid = models.CharField(max_length=10, choices=JABATAN_CHOICES)
    alamat = models.TextField()
    foto = models.ImageField(upload_to='gambar/', null=True)
    created_at = models.DateTimeField('Cretaed at',default=timezone.now)

    class Meta:
        ordering = ['-created_at']

    def __str__(self):
        return self.nama
