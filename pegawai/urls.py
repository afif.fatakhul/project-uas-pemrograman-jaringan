from django.urls import path
from .views import *
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('pegawai',index,name='pegawai'),
    path('pegawai/add',tambah_pegawai, name='pegawai_add'),
    path('pegawai/edit/<int:id_pegawai>', ubah_pegawai, name='ubah_pegawai'),
    path('pegawai/hapus/<int:id_pegawai>', hapus_pegawai, name='hapus_pegawai'),
]


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)