from django import forms
from django.forms import ModelForm
from .models import Pegawai

class FormPegawai(ModelForm):
	class Meta:
		model = Pegawai
		fields = '__all__'

		widgets = {
            'nip' : forms.TextInput({'class':'form-control'}),
			'nama' : forms.TextInput({'class':'form-control'}),
            'jabatanid' : forms.Select({'class':'form-select'}),
			'alamat' : forms.Textarea({'class':'form-control'}),
            'created_at' : forms.DateTimeInput({'class':'form-control'}),
		}