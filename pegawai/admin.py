from django.contrib import admin
from .models import Pegawai

# Register your models here.
@admin.register(Pegawai)
class PegawaiAdmin(admin.ModelAdmin):
    list_display = ['nip','nama','jabatanid','alamat']
    list_filter = ['nip','nama','alamat']
    search_fields = ['nip','nama','alamat']